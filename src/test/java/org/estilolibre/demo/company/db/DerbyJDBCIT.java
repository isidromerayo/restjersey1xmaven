package org.estilolibre.demo.company.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author isidromerayo
 */
public class DerbyJDBCIT {

    public DerbyJDBCIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws SQLException {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void helloInMemoryJDBC() throws SQLException {
        // OLD Style
        String dbUrl = "jdbc:hsqldb:mem:demodb";
        Connection conn = DriverManager.getConnection(dbUrl);

        //
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("Create table users (id int primary key, name varchar(30))");

        // insert 2 rows
        stmt.executeUpdate("insert into users values (1,'tom')");
        stmt.executeUpdate("insert into users values (2,'peter')");

        // query
        ResultSet rs = stmt.executeQuery("SELECT * FROM users");
        int count = 0;
        while (rs.next()) {
            count++;
        }
        int expected = 2;
        assertEquals(expected, count);
    }
}
